package com.example.homework22

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LoginViewModel: ViewModel() {
    val progressBarVisible = MutableLiveData<Boolean>()
    val result = MutableLiveData<String>()

    fun logIn(email: String, password: String){
        progressBarVisible.value = true
        Handler().postDelayed({
            progressBarVisible.value = false
            if(email.isNotEmpty() && password.isNotEmpty()){
                result.value = "Success"
            }else result.value ="Failure"
        }, 3000)

    }
}