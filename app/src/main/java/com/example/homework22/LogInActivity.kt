package com.example.homework22

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.homework22.databinding.ActivityLoginBinding
import kotlinx.android.synthetic.main.activity_login.*
import java.util.regex.Pattern


class LogInActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val model = ViewModelProvider(this)[LoginViewModel::class.java]
        val binding: ActivityLoginBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.logInModel = model
        binding.lifecycleOwner = this


        passwordEditText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }


            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                updatePasswordStrengthView(s.toString())
            }
        })
    }


    private fun updatePasswordStrengthView(password: String) {
        if (TextView.VISIBLE != password_strength.visibility)
            return

        if (TextUtils.isEmpty(password)) {
            password_strength.text = ""
            progressBarLine.progress = 0
            return
        }

        val str = PasswordStrength.calculateStrength(password)
        password_strength.text = str.getText(this)
        password_strength.setTextColor(str.color)

        progressBarLine.progressDrawable.setColorFilter(
            str.color,
            android.graphics.PorterDuff.Mode.SRC_IN
        )
        when {
            str.getText(this) == "Weak" -> {
                progressBarLine.progress = 25
            }
            str.getText(this) == "Medium" -> {
                progressBarLine.progress = 50
            }
            str.getText(this) == "Strong" -> {
                progressBarLine.progress = 75
            }
            else -> {
                progressBarLine.progress = 100
            }
        }
    }

}
